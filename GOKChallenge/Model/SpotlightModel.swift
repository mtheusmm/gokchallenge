//
//  SpotlightModel.swift
//  GOKChallenge
//
//  Created by Matheus Moreira on 25/08/20.
//  Copyright © 2020 Matheus de Melo Moreira. All rights reserved.
//

import Foundation

struct SpotlightModel: Decodable {
    let name: String
    let bannerURL: String
    let description: String
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case bannerURL = "bannerURL"
        case description = "description"
    }
}
