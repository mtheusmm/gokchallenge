//
//  MainResponse.swift
//  GOKChallenge
//
//  Created by Matheus Moreira on 26/08/20.
//  Copyright © 2020 Matheus de Melo Moreira. All rights reserved.
//

import Foundation

struct MainResponse: Decodable {
    let spotlight: [SpotlightModel]
    let products: [ProductModel]
    let cash: CashModel
    
    enum CodingKeys: String, CodingKey {
        case spotlight = "spotlight"
        case products = "products"
        case cash = "cash"
    }
}
