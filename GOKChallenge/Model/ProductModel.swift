//
//  ProductModel.swift
//  GOKChallenge
//
//  Created by Matheus Moreira on 25/08/20.
//  Copyright © 2020 Matheus de Melo Moreira. All rights reserved.
//

import Foundation

struct ProductModel: Decodable {
    let name: String
    let imageURL: String
    let description: String
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case imageURL = "imageURL"
        case description = "description"
    }
}
