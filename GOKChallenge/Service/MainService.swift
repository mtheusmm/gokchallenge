//
//  MainService.swift
//  GOKChallenge
//
//  Created by Matheus Moreira on 25/08/20.
//  Copyright © 2020 Matheus de Melo Moreira. All rights reserved.
//

import Alamofire
import Foundation

protocol MainServiceDelegate {
    func didGetData(response: MainResponse)
}

protocol MainServiceProtocol {
    var delegate: MainServiceDelegate? { get set }
}

class MainService: MainServiceProtocol {

    var delegate: MainServiceDelegate?
    
    func getJSON() {
        let request = AF.request("https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/sandbox/products")
        request.responseDecodable(of: MainResponse.self) { (response) in
            guard let data = response.value else { return }
            self.delegate?.didGetData(response: data)
        }
    }

}
