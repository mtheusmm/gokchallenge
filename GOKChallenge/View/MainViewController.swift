//
//  MainViewController.swift
//  GOKChallenge
//
//  Created by Matheus Moreira on 26/08/20.
//  Copyright © 2020 Matheus de Melo Moreira. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    private let viewMain: MainView = MainView()
    var returnedData: MainResponse?
    var presenterMain: MainViewControllerPresenterProtocol?

    override func loadView() {
        super.loadView()
        view.addSubview(viewMain)
        viewMain.snp.makeConstraints({ (make) in
            make.top.trailing.bottom.leading.equalToSuperview()
        })
        self.view.backgroundColor = .white
        setupPresenter()
        presenterMain?.getJSONData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        viewMain.delegate = self
    }
    
    func setupPresenter() {
        self.presenterMain = MainFactory.create(view: self)
        
    }

}

extension MainViewController: MainViewDelegate {
    // Put your code here
}

extension MainViewController: MainViewControllerProtocol {
    func getedData(response: MainResponse) {
        self.viewMain.data = response
        self.viewMain.addSpotlights()
        self.viewMain.addCashArea()
        self.viewMain.addProducts()
        self.returnedData = response
        
    }
    
}
