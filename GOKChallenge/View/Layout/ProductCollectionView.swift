//
//  ProductCollectionView.swift
//  GOKChallenge
//
//  Created by Matheus Moreira on 26/08/20.
//  Copyright © 2020 Matheus de Melo Moreira. All rights reserved.
//

import Foundation
import Kingfisher
import SnapKit

class ProductCollectionView: UICollectionView, UICollectionViewDataSource, UICollectionViewDelegate {
    
    public var list: [ProductModel]?
    let cellID = "Product"
    
    public override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        setupView()
        setupAditionalConfiguration()
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
        setupAditionalConfiguration()
    }
    
    func setupView() {
        dataSource = self
        delegate = self
        self.backgroundColor = .white
        self.showsHorizontalScrollIndicator = false
    }
    
    func setupAditionalConfiguration() {
        self.register(ProductCell.self, forCellWithReuseIdentifier: cellID)
        self.isAccessibilityElement = false
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return list?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! ProductCell
        let imgURL = URL(string: (list?[indexPath.row].imageURL)!)
        let image = UIImageView()
        image.kf.setImage(with: imgURL) {
            result in
            switch result {
            case .success(let value):
                print("SUCESSO: \(value.source.url?.absoluteString ?? "")")
                cell.setImage(image: image)
            case .failure(let error):
                print("FALHA: \(error.localizedDescription)")
                self.list?.remove(at: indexPath.row)
                collectionView.reloadData()
            }
        }
        
        return cell
    }
    
    
}
