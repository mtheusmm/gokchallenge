//
//  ProductCell.swift
//  GOKChallenge
//
//  Created by Matheus Moreira on 26/08/20.
//  Copyright © 2020 Matheus de Melo Moreira. All rights reserved.
//

import Foundation
import SnapKit
import Kingfisher

class ProductCell: UICollectionViewCell {
    
    public var productImage = UIImageView()
    public var viewImgContainer: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        createSubviews()
    }
    
    private func createSubviews() {
        viewImgContainer = UIView()
        viewImgContainer.layer.cornerRadius = 10.0
        viewImgContainer.layer.shadowColor = UIColor.black.cgColor
        viewImgContainer.layer.shadowOffset = .zero
        viewImgContainer.layer.shadowRadius = 5.0
        viewImgContainer.layer.shadowOpacity = 0.5
        viewImgContainer.backgroundColor = .white
        addSubview(viewImgContainer)
        viewImgContainer.snp.makeConstraints { (make) in
            make.top.centerX.equalToSuperview()
            make.height.width.equalTo(120.0)
            
        }
    }
    
    public func setImage(image: UIImageView) {
        productImage = image
//        let url = URL(string: imgURL)
//        productImage.kf.setImage (with: url) {
//            result in
//            switch result {
//            case .success(let value):
//                print("SUCESSO: \(value.source.url?.absoluteString ?? "")")
//            case .failure(let error):
//                print("FALHA: \(error.localizedDescription)")
//                self.isHidden = true
//            }
//        }
        productImage.layer.cornerRadius = 10.0
        productImage.layer.masksToBounds = true
        viewImgContainer.addSubview(productImage)
        productImage.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.height.width.equalTo(60)
        }
    }
}
