//
//  MainView.swift
//  GOKChallenge
//
//  Created by Matheus Moreira on 26/08/20.
//  Copyright © 2020 Matheus de Melo Moreira. All rights reserved.
//

import UIKit
import SnapKit

protocol MainViewDelegate {

}

class MainView: UIView {

    var data: MainResponse?
    var delegate: MainViewDelegate?
    var header: UIView!
    var spotlightCollectionView: SpotlightCollectionView!
    var productCollectionView: ProductCollectionView!
    var cashAreaView: UIView!
    var productsAreaView: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        createSubviews()
    }
    
    private func createSubviews() {
        header = UIView()
        header.backgroundColor = .white
        addSubview(header)
        header.snp.makeConstraints { (make) in
            make.leading.trailing.top.equalToSuperview()
            make.height.equalTo(60.0)
        }
        let headerName = UILabel()
        headerName.text = "Olá, Maria"
        headerName.backgroundColor = .white
        headerName.font = UIFont.boldSystemFont(ofSize: 16.0)
        headerName.textColor = .black
        header.addSubview(headerName)
        headerName.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(16.0)
            make.bottom.equalToSuperview()
        }
        
        self.backgroundColor = .white
        
    }
    
    func addSpotlights() {
        UIView.animate(withDuration: 0.3, animations: {
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 10, left: 16, bottom: 8, right: 16)
            layout.itemSize = CGSize(width: UIScreen.main.bounds.width - 40.0, height: 180)
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 15
            self.spotlightCollectionView = SpotlightCollectionView(frame: .zero, collectionViewLayout: layout)
            self.spotlightCollectionView.list = self.data?.spotlight
            self.addSubview(self.spotlightCollectionView)
            self.spotlightCollectionView.snp.makeConstraints { (make) in
                make.top.equalTo(self.header.snp.bottom)
                make.leading.trailing.equalToSuperview()
                make.height.equalTo(220.0)
            }
        })
    }
    
    func addCashArea() {
        UIView.animate(withDuration: 0.3, animations: {
            self.cashAreaView = UIView()
            self.cashAreaView.backgroundColor = .white
            self.addSubview(self.cashAreaView)
            self.cashAreaView.snp.makeConstraints { (make) in
                make.top.equalTo(self.spotlightCollectionView.snp.bottom)
                make.leading.trailing.equalToSuperview()
                make.height.equalTo(160)
            }
            
            let cashTitle = UILabel()
            cashTitle.text = "digio Cash"
            cashTitle.textColor = .black
            cashTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
            self.cashAreaView.addSubview(cashTitle)
            cashTitle.snp.makeConstraints { (make) in
                make.top.equalToSuperview().offset(10.0)
                make.leading.equalToSuperview().offset(16.0)
            }
            
            let cashImg = UIImageView()
            let cashImgURL = URL(string: (self.data?.cash.bannerURL)!)
            cashImg.kf.setImage(with: cashImgURL)
            cashImg.layer.cornerRadius = 10.0
            cashImg.layer.masksToBounds = true
            self.cashAreaView.addSubview(cashImg)
            cashImg.snp.makeConstraints { (make) in
                make.height.equalTo(100.0)
                make.top.equalTo(cashTitle.snp.bottom).offset(8.0)
                make.leading.equalToSuperview().offset(16.0)
                make.trailing.equalToSuperview().offset(-16.0)
            }
        })
    }
    
    func addProducts() {
        UIView.animate(withDuration: 0.3, animations: {
            self.productsAreaView = UIView()
            self.productsAreaView.backgroundColor = .white
            self.addSubview(self.productsAreaView)
            self.productsAreaView.snp.makeConstraints { (make) in
                make.top.equalTo(self.cashAreaView.snp.bottom)
                make.leading.trailing.equalToSuperview()
                make.height.equalTo(450.0)
            }
            
            let productsTitle = UILabel()
            productsTitle.text = "Produtos"
            productsTitle.textColor = .black
            productsTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
            self.productsAreaView.addSubview(productsTitle)
            productsTitle.snp.makeConstraints { (make) in
                make.top.equalToSuperview().offset(10.0)
                make.leading.equalToSuperview().offset(16.0)
            }
            
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16)
            layout.itemSize = CGSize(width: 120, height: 120)
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 20
            self.productCollectionView = ProductCollectionView(frame: .zero, collectionViewLayout: layout)
            self.productCollectionView.list = self.data?.products
            self.productsAreaView.addSubview(self.productCollectionView)
            self.productCollectionView.snp.makeConstraints { (make) in
                make.top.equalTo(productsTitle.snp.bottom).offset(8.0)
                make.leading.trailing.equalToSuperview()
                make.height.equalTo(140)
            }
        })
    }
}
