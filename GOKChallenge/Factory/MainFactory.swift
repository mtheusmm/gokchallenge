//
//  MainFactory.swift
//  GOKChallenge
//
//  Created by Matheus Moreira on 25/08/20.
//  Copyright © 2020 Matheus de Melo Moreira. All rights reserved.
//

import Foundation

class MainFactory {
    public static func create<VIEW: MainViewControllerProtocol>(view: VIEW) -> MainViewControllerPresenterProtocol {
        return MainPresenter(view: view, service: MainService())
    }
}
