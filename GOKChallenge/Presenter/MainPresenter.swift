//
//  MainPresenter.swift
//  GOKChallenge
//
//  Created by Matheus Moreira on 25/08/20.
//  Copyright © 2020 Matheus de Melo Moreira. All rights reserved.
//

import Alamofire
import Foundation

protocol MainViewControllerProtocol {
    func getedData(response: MainResponse)
}

protocol MainViewControllerPresenterProtocol {
    func getJSONData()
}

class MainPresenter<VIEW: MainViewControllerProtocol>: MainViewControllerPresenterProtocol {
    
    var view: VIEW
    var service: MainService!
    
    init(view: VIEW, service: MainService) {
        self.view = view
        self.service = service
        self.service.delegate = self
    }
    
    func getJSONData() {
        service.getJSON()
    }
}

extension MainPresenter: MainServiceDelegate {
    func didGetData(response: MainResponse) {
        self.view.getedData(response: response)
    }
}
